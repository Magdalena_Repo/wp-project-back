import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav-bar.component.html',
  styleUrls: ['./main-nav-bar.component.css']
})
export class MainNavBarComponent implements OnInit {

  loggedInUser: string = null;
  isLoggedIn: boolean;
  @Input() typeOfUser: string;

  constructor( private router: Router) { }

  ngOnInit() {
    this.loggedInUser = sessionStorage.getItem("loggedInUser");
  }
  onLogin(){
    this.isLoggedIn = true;
    this.router.navigate(['login']);
  }
  onLogout() {
    this.isLoggedIn = false;
    sessionStorage.removeItem('auth-user');
    sessionStorage.removeItem('type');
    sessionStorage.removeItem('loggedInUser');
    this.typeOfUser = null;
    this.router.navigate(['/login']);
  }

}
