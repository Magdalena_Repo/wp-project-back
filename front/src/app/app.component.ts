import {Component, Input, OnInit, Output, ViewChildren} from '@angular/core';
import {User} from './models/user';
import {UserService} from './services/user-service';
import {LoginComponent} from './login/login.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Reservations Online';
  @Input() loggedInUser = null;
  @ViewChildren(LoginComponent) child : LoginComponent;
  user: User = new User();
  constructor(private userService: UserService){}

  ngOnInit(){
    let username = sessionStorage.getItem('loggedInUser');
    if(username){
      this.userService.getUserByUsername(username).subscribe(data => {
        this.user = data;
      });
    }
  }

  theUserIsLoggedIn(user: User){
    console.log('heeeeeey I am Hereeee');
    this.user = user;
  }
}
