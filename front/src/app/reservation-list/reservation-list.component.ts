import { Component, OnInit } from '@angular/core';
import {Company} from '../models/company';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../services/user-service';
import {CompanyService} from '../services/company-service';
import {ReservationService} from '../services/reservation-service';
import {User} from '../models/user';
import {Reservation} from '../models/reservation';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {

  selectedCompany: Company = null;
  user: User = new User();
  companies: Company[] = [];
  reservations: Reservation[] = [];
  reservationToBeEdited: boolean = false;
  message: string = null;
  reserveMatCard:boolean = false;
  newReservation: Reservation = new Reservation();

  constructor(private route : ActivatedRoute, private userService: UserService, private companyService: CompanyService, private reservationService: ReservationService) { }

  ngOnInit() {
    this.userService.getUserByUsername(sessionStorage.getItem('loggedInUser')).subscribe(data => {
      this.user = data;
      if(this.user.type === 'owner'){
        this.companyService.getCompaniesByOwner(this.user.id).subscribe(companies => {
          this.companies = companies;
        });
      } else{
        this.reservationService.userReservations(this.user.id).subscribe(reservations => {
          this.reservations = reservations;
        });
      }
    });
  }
  getAllReservationsForRestoraunt(companyId: number){
    this.reservationService.companyReservations(companyId).subscribe(data => {
      this.reservations = data;
    });
  }
  editReservation(){
    this.reservationToBeEdited = true;
  }
  edit(reservation: Reservation){
    this.reservationService.editReservation(reservation).subscribe(data => {
      reservation = data;
      this.reservationToBeEdited = false;
    });
  }

  deleteReservation(reservation: Reservation){
      this.reservationService.deleteReservation(reservation.id).subscribe(data => {
        this.message ='succesfully deleted!';
      });
  }
  reserve(){
    this.reserveMatCard = true;
    this.companyService.getCompanies().subscribe(companies => {
      this.companies = companies;
    });
  }
  submitTheReservation(newReservation: Reservation){
    this.reservationService.reserve(newReservation, this.user.id).subscribe(data => {
      this.reservationService.userReservations(this.user.id).subscribe(rez => {
        this.reservations = rez;
        this.reserveMatCard = false;
        this.reservationToBeEdited = false;
        this.newReservation = new Reservation();
      });
    });
  }
}
