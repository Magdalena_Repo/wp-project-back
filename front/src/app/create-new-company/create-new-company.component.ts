import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../models/user';
import {Company} from '../models/company';
import {CompanyService} from '../services/company-service';
import {CompanyType} from '../models/company-type';
import {UserService} from '../services/user-service';
import {CompanyImage} from '../models/company-image';
import {Place} from '../models/place';

@Component({
  selector: 'app-create-new-company',
  templateUrl: './create-new-company.component.html',
  styleUrls: ['./create-new-company.component.css']
})
export class CreateNewCompanyComponent implements OnInit {

  user: User;
  company: Company = new Company();
  companyTypes: CompanyType[] = [];
  companyPlaces: Place[] = [];
  imgUrl: string = '';
  createdCompany: Company = new Company();

  constructor(private route: ActivatedRoute, private companyService:CompanyService, private userService:UserService, private router:Router) { }

  ngOnInit() {
    this.companyService.getCompanyTypes().subscribe(data => {
      this.companyTypes = data;
      this.userService.getUserByUsername(sessionStorage.getItem('loggedInUser')).subscribe(info => {
        this.user = info;
        this.company.ownerId = this.user.id;
      });
      this.companyService.getCompanyPlaces().subscribe(places =>{
        this.companyPlaces = places;
      });
    });

  }
  comapnyImageUrl(url: string) {
    const companyImg: CompanyImage = new CompanyImage(url);
    this.company.images.push(companyImg);
  }
  createCompany() {
    this.companyService.createCompany(this.company).subscribe(newCompany =>{
      this.createdCompany = newCompany;
      console.log(this.createdCompany);
      this.router.navigate(['/companies']);
    });
  }
}
