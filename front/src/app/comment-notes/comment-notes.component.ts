import { Component, OnInit } from '@angular/core';
import {Note} from '../models/note';
import {NoteService} from '../services/note-service';

@Component({
  selector: 'app-comment-notes',
  templateUrl: './comment-notes.component.html',
  styleUrls: ['./comment-notes.component.css']
})
export class CommentNotesComponent implements OnInit {
  notesFromUsers: Note[] = [];

  constructor(private noteService: NoteService) { }

  ngOnInit() {
    this.noteService.notesFromOtherPeople().subscribe(notes =>{
      this.notesFromUsers = notes;
    });
  }

}
