import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentNotesComponent } from './comment-notes.component';

describe('CommentNotesComponent', () => {
  let component: CommentNotesComponent;
  let fixture: ComponentFixture<CommentNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
