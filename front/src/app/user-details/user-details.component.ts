import { Component, OnInit } from '@angular/core';
import {User} from '../models/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  user: User;

  constructor() { }

  ngOnInit() {
    if(sessionStorage.getItem('reload') === 'true') {
      window.location.reload();
      sessionStorage.removeItem('reload');
    }
  }

}
