import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Reservation} from '../models/reservation';
import {Observable} from 'rxjs';
import {Company} from '../models/company';

@Injectable({
  providedIn: 'root',
})
export class ReservationService {

  usersUrl: string = "http://localhost:8080/reservations";

  constructor(private http: HttpClient) {
  }


  reserve(newReservation: Reservation, userId: number): Observable<Reservation> {
    return this.http.post<Reservation>(this.usersUrl + '/' + userId + '/create', newReservation);
  }

  companyReservations(companyId: number): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(this.usersUrl + '/company/' + companyId);
  }

  companyReservationsByDate(companyId: number, date: any): Observable<Reservation[]> {
    return this.http.get<Reservation[]>('/api/reserve/' + companyId + '/' + date.year + '-' + date.month + '-' + date.day);
  }

  userReservations(userId: number): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(this.usersUrl + '/user/' + userId);
  }
  editReservation(reservation: Reservation): Observable<Reservation> {
    return this.http.put<any>(this.usersUrl + '/' + reservation.id + '/update', reservation);
  }
  deleteReservation(reservationId: number){
    return this.http.delete(this.usersUrl + '/' + reservationId + '/deleteReservation');
  }
}
