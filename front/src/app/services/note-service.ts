import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Reservation} from '../models/reservation';
import {Note} from '../models/note';

@Injectable({
  providedIn: 'root',
})
export class NoteService {

  usersUrl: string = "http://localhost:8080/users";

  constructor(private http: HttpClient) {
  }

  notesFromOtherPeople(): Observable<Note[]> {
    return this.http.get<Note[]>(this.usersUrl + '/notes/all');
  }
}
