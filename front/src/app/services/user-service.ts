
import {User} from '../models/user';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Note} from '../models/note';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  usersUrl: string = "http://localhost:8080/users";
  isLogedIn: boolean = false;
  logedIn: string;

  constructor(private http: HttpClient){}

  loginUser(username: string, pass: string):any{

    const body ="grant_type=password&"+'username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(pass);

    return this.http.post<any>('http://localhost:8080/oauth/token',body,{

      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization','Basic '+btoa('my_reservations:myAdmin'))
    });
  }

  getUserById( userId: number) {
    return this.http.get<User>(
      this.usersUrl  + userId);
  }
  getUserByUsername(username: string) {
      return this.http.get<User>(
        this.usersUrl+'?userEmail='+username);
  }

  registerUser(newUser: User): Observable<User> {
    return this.http.post<User>(
      this.usersUrl + '/register', newUser);
  }
}

