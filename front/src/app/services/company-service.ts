import {HttpClient} from '@angular/common/http';
import {Company} from '../models/company';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {CompanyType} from '../models/company-type';
import {Place} from '../models/place';

@Injectable({
  providedIn: 'root',
})
export class CompanyService {

  usersUrl: string = "http://localhost:8080/companies";

  constructor(private http: HttpClient){}

  // saveCompany(name: string, address: string, description: string, capacity: number,
  //               place: Place, companyType: CompanyType,
  //               companyImages: CompanyImage[], workingDaysMask: string):Observable<Company> {
  //   let company = new Company(name, address, description, capacity, place, companyType, companyImages, workingDaysMask);
  //   return this.http.post<Company>('/api/owner/add/company', company);
  // }

  getCompaniesByType(type: string): Observable<Company[]> {
    return this.http.get<Company[]>(this.usersUrl + '?type=' + type);
  }

  getCompaniesByPlace(place: string): Observable<Company[]> {
    return this.http.get<Company[]>(this.usersUrl + '?place=' + place);
  }

  getCompanyByName(name: string): Observable<Company> {
    return this.http.get<Company>(this.usersUrl + '/getAllByName?name=' + name);
  }

  getCompaniesByOwner(ownerId: number) : Observable<Company[]> {
    return this.http.get<Company[]>(this.usersUrl + '/' + ownerId);
  }
  getCompanies() : Observable<Company[]> {
    return this.http.get<Company[]>(this.usersUrl + "/all");
  }
  getCompanyTypes(): Observable<CompanyType[]> {
    return this.http.get<CompanyType[]>(this.usersUrl + "/types");
  }
  getCompanyPlaces(): Observable<Place[]> {
    return this.http.get<Place[]>(this.usersUrl + "/places");
  }
  createCompany(newCompany: Company): Observable<Company> {
    return this.http.post<Company>(this.usersUrl + '/create', newCompany);
  }

}
