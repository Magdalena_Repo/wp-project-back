import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserService} from '../services/user-service';
import {User} from '../models/user';
import {Router} from '@angular/router';
import {Owner} from '../models/owner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  error: string = null;
  username: string = '';
  password: string = '';
  typeOfUser: string = '';
  hide = true;
  @Output() loggedInUser: EventEmitter<any> = new EventEmitter();
  loggedInUserValue: User = null;
  // loggedInOwnerValue: Owner = null;
  // user_roles: any = [{type: "guest"},{type: "owner"}];

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {

  }

  login(){
    this.userService.loginUser(this.username, this.password).subscribe(info => {
      sessionStorage.setItem("auth-user", info.access_token);
      sessionStorage.setItem("loggedInUser", this.username);
      sessionStorage.setItem("type", this.typeOfUser);
      if (sessionStorage.getItem('auth-user') !== null) {
        this.userService.getUserByUsername(this.username).subscribe(data => {
          this.loggedInUserValue = data;
          this.loggedInUser.emit(data);
          sessionStorage.setItem('reload',true+'');
          this.router.navigate(['users/details/' + this.loggedInUserValue.id]);
        });
      }
      if (sessionStorage.getItem('auth-user') === null){
          this.error = "Invalid credentials!!";
      }
    },error => {
      this.error = 'Invalid credentials! Please enter the correct information.';
    });
  }

  show(){
    this.hide = !this.hide;
  }
}
