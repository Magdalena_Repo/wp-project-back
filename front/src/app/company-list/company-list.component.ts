import {Component, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CompanyService} from '../services/company-service';
import {User} from '../models/user';
import {UserService} from '../services/user-service';
import {Company} from '../models/company';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit, OnChanges {

  user: User = new User();
  companies: Company[] = [];
  constructor(private route: ActivatedRoute, private companyService: CompanyService, private userService: UserService, private router: Router) { }

  ngOnInit() {
    const owned: string = this.route.snapshot.queryParamMap.get('owned');
    this.userService.getUserByUsername(sessionStorage.getItem('loggedInUser')).subscribe(data => {
      this.user = data;
      console.log("this is user",data);
      if(owned === 'true'){
        this.companyService.getCompaniesByOwner(this.user.id).subscribe(companies => {
          this.companies = companies;
          console.log("companieeeeees",companies);
        });
      }else{
        this.companyService.getCompanies().subscribe(companies => {
          this.companies = companies;
        });
      }
    });
  }
  ngOnChanges(){
    const owned: string = this.route.snapshot.queryParamMap.get('owned');
    this.userService.getUserByUsername(sessionStorage.getItem('loggedInUser')).subscribe(data => {
      this.user = data;
      console.log("this is user",data);
      if(owned === 'true'){
        this.companyService.getCompaniesByOwner(this.user.id).subscribe(companies => {
          this.companies = companies;
          console.log("companieeeeees",companies);
        });
      }else{
        this.companyService.getCompanies().subscribe(companies => {
          this.companies = companies;
        });
      }
    });
  }
  createNewCompany() {
    this.router.navigate(['users/' + this.user.id + '/company/create']);
  }
}
