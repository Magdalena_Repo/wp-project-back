import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavBarComponent } from './main-nav-bar/main-nav-bar.component';
import {
  MatButtonModule, MatCardModule, MatDatepickerModule,
  MatDividerModule, MatExpansionModule, MatFormFieldControl, MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatListItem, MatListModule,
  MatMenu,
  MatMenuModule, MatNativeDateModule,
  MatNavList, MatOptionModule, MatProgressSpinnerModule, MatSelectModule,
  MatSidenavModule, MatTableModule,
  MatToolbarModule
} from '@angular/material';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import {AuthInterceptor} from './helpers/auth-interceptor';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import {UserService} from './services/user-service';
import { UserDetailsComponent } from './user-details/user-details.component';
import { CompanyListComponent } from './company-list/company-list.component';
import {CompanyService} from './services/company-service';
import { ReservationListComponent } from './reservation-list/reservation-list.component';
import {ReservationService} from './services/reservation-service';
import { CommentNotesComponent } from './comment-notes/comment-notes.component';
import {NoteService} from './services/note-service';
import { RegisterComponent } from './register/register.component';
import { CreateNewCompanyComponent } from './create-new-company/create-new-company.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavBarComponent,
    LoginComponent,
    HomeComponent,
    UserDetailsComponent,
    CompanyListComponent,
    ReservationListComponent,
    CommentNotesComponent,
    RegisterComponent,
    CreateNewCompanyComponent  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatExpansionModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule
],
  providers: [UserService, CompanyService, ReservationService, NoteService, { provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
