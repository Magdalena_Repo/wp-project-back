import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {CompanyListComponent} from './company-list/company-list.component';
import {ReservationListComponent} from './reservation-list/reservation-list.component';
import {CommentNotesComponent} from './comment-notes/comment-notes.component';
import {RegisterComponent} from './register/register.component';
import {CreateNewCompanyComponent} from './create-new-company/create-new-company.component';

const routes: Routes = [
  {path:'home',  component: HomeComponent},
  {path:'login', component: LoginComponent},
  {path:'register', component: RegisterComponent },
  {path:'users/details/:id', component: UserDetailsComponent},
  {path:'companies', component: CompanyListComponent},
  {path:'reservations', component: ReservationListComponent},
  {path:'notes', component: CommentNotesComponent},
  {path:'users/:id/company/create', component: CreateNewCompanyComponent},
  {path:'', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
