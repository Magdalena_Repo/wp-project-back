import {Company} from './company';
import {User} from './user';

export class Reservation {
  id: number
  remark: String;
  personCount: number;
  forDate: Date;
  userId: number;
  userEmail : string;
  userFirstName: string;
  userLastName: string;
  companyId: number;
  companyName: string;
  companyAddress: string;


  constructor(remark: String= null, personCount: number= null, date: Date= null) {
    this.remark = remark;
    this.personCount = personCount;
    this.forDate = date;
  }
}
