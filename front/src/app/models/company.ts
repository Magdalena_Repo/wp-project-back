import {CompanyImage} from './company-image';

export class Company {
  id: number;
  name: string;
  address: string;
  description: string;
  capacity: number;
  placeId: number;
  companyTypeId: number;
  workingDaysMask: string;
  images: CompanyImage[];
  ownerId: number;


  constructor(name: string=null, address: string=null, description: string=null,
              capacity: number=null, placeId: number=null, companyTypeId:
                number=null, images: CompanyImage[]=[], workingDaysMask: string=null, ownerId: number=null) {
    this.name = name;
    this.address = address;
    this.description = description;
    this.capacity = capacity;
    this.placeId = placeId;
    this.companyTypeId = companyTypeId;
    this.images = images;
    this.workingDaysMask = workingDaysMask;
    this.ownerId = ownerId;

  }
}
