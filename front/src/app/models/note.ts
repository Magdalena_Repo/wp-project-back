export class Note {
  id: number;
  subject: string;
  description: string;
  userId: number;
  companyId: number;
  companyName: string;
}
