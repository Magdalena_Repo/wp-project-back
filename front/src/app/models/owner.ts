export class Owner {
  firstName: String;
  lastName: String;
  email: String;
  password: String;
  birthDate: String;
  type:String;
  image:String;

  constructor(firstName: String, lastName: String, email: String, password: String,birthDate: String, type:String, image:String) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.birthDate = birthDate;
    this.type = type;
    this.image = image;
  }
}
