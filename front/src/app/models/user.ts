export class User {

  id: number
  firstName: String;
  lastName: String;
  email: String;
  password: String;
  birthDate: String;
  type: String;
  image: String;


  constructor(firstName: String =null, lastName: String=null, email: String=null, password: String=null, birthDate: String=null, type:String=null, image: String=null) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.birthDate = birthDate;
    this.type = type;
    this.image= image;
  }
}
