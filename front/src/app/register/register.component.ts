import { Component, OnInit } from '@angular/core';
import {User} from '../models/user';
import {UserService} from '../services/user-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  newUser: User = new User();
  user_roles: string[] = ["guest","owner"];

  constructor(private userService:UserService, private router:Router) { }

  ngOnInit() {
  }

  register(newUser: User){
    this.userService.registerUser(newUser).subscribe(data => {
      this.newUser = data;
      this.router.navigate(['login/'] );
    });
  }
}
