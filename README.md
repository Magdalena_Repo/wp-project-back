# WP-Project-Back

Тема на проктот: 
Резервациски систем по предметот Веб програмирање


Содржина: 
Имаме два типа на корисници: гости и сопственици на ресторани/барови/ланч барови итн.
Секој корисник најпрво се регистрира на порталот.
Потоа истиот се најавува и може да ги користи сите поволности.
Гостите ќе може да прегледуваат ресторани/барови, да резервираат, да едитираат стари резервации.
Сопствениците ќе имаат можност да ги пребаруваат своите ресторанти коишто ги поседуваат и да креаираат нови пропертиња.


Како пример корисник за гостин, пробајте да се логирате со:
username: user2@gmail.com 
password: user

Како пример корисник за сопственик, пробајте да се логирате со:
username: user1@gmail.com 
password: user


Упатство:

1.Пред да ја пуштите спринг апликацијата со име: reservations, треба да имате инсталирано MySQL сервер.


2.Kреаирајте инстанца со username: root со password: test.


3.Креаирајте празна база/шема со име test.


4. Clean, Compile & Debug!


5 Извршете ги следниве квериња!!!!

Querry 1:

CREATE TABLE oauth_access_token (
    authentication_id varchar(255) NOT NULL PRIMARY KEY,
    token_id varchar(255) NOT NULL,
    token blob NOT NULL,
    user_name varchar(255) NOT NULL,
    client_id varchar(255) NOT NULL,
    authentication blob NOT NULL,
    refresh_token varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


Querry 2:

CREATE TABLE oauth_refresh_token (
    token_id varchar(255) NOT NULL,
    token blob NOT NULL,
    authentication blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	

6. За да го стартувате фронт-от на апликацијата со име: font, треба да ја извршите следнава команга npm install и потос ng serve.

