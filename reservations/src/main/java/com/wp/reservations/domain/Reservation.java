package com.wp.reservations.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "reservation")
@Data
@EqualsAndHashCode
public class Reservation{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "deleted",columnDefinition = "boolean default false")
    private Boolean deleted;

    @Column(name = "canceled",columnDefinition = "boolean default false")
    private Boolean canceled;

    @Column(name = "remark",length = 512)
    private String remark;

    @Column(name = "person_count")
    private Integer personCount;

    @Column(name = "for_date", nullable = false)
    private LocalDateTime forDate;

    @JoinColumn(name = "user_id")
    @ManyToOne(optional = false)
    private User user;

    @JoinColumn(name = "company_id")
    @ManyToOne(optional = false)
    private Company company;
}
