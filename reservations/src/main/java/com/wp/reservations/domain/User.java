package com.wp.reservations.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = {"notes", "reservations"})
@Table(name = "user")
public class User extends BaseUserEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Place place;

    @OneToMany(mappedBy = "user", cascade=CascadeType.ALL)
    private Set<Note> notes =  new HashSet<>();

    @OneToMany(mappedBy = "user", cascade=CascadeType.ALL)
    private Set<Reservation> reservations =  new HashSet<>();


}
