package com.wp.reservations.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = {"images","reservations","notes"})
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "capacity")
    private Integer capacity;

    @Column(name = "working_days_mask")
    private String workingDaysMask;

    @Column(name = "deleted")
    private Boolean deleted;

    @ManyToOne(optional = false)
    private Place place;

    @OneToMany(mappedBy = "company", cascade=CascadeType.ALL)
    private Set<CompanyImage> images = new HashSet<>();

    @ManyToOne(optional = false)
    @JoinColumn(name = "company_type_id")
    private CompanyType companyType;

    @OneToMany(mappedBy = "company", cascade=CascadeType.ALL)
    private Set<Note> notes =  new HashSet<>();

    @ManyToOne(optional = false)
    @JoinColumn(name = "owner_id")
    private User owner;

    @OneToMany(mappedBy = "company", cascade=CascadeType.ALL)
    private Set<Reservation> reservations =  new HashSet<>();
}
