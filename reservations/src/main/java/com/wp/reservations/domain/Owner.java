package com.wp.reservations.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.*;
import java.util.*;

@Entity(name = "owner")
@EqualsAndHashCode
@Data
public class Owner extends BaseUserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "co_owner",nullable = false)
    private Boolean coOwner;


}
