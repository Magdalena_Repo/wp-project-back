package com.wp.reservations.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseUserEntity {

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "birth_date")
    private LocalDateTime birthDate;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "type_of_user", nullable = false)
    private String type;

    @Column(name = "user_image")
    private String image;
}

