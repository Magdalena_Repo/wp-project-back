package com.wp.reservations.seed;

import com.wp.reservations.domain.*;
import com.wp.reservations.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
@Transactional
public class BootstrapLoader implements ApplicationListener<ContextRefreshedEvent> {

    private final UserRepository userRepository;
    private final OwnerRepository ownerRepository;
    private final CompanyRepository companyRepository;
    private final PlaceRepository placeRepository;
    private final ReservationRepository reservationRepository;
    private final NoteRepository noteRepository;
    private final CompanyTypeRepository companyTypeRepository;
    private final CompanyImageRepository companyImageRepository;

    public BootstrapLoader(UserRepository userRepository, OwnerRepository ownerRepository, CompanyRepository companyRepository, PlaceRepository placeRepository, ReservationRepository reservationRepository, NoteRepository noteRepository, CompanyTypeRepository companyTypeRepository, CompanyImageRepository companyImageRepository) {
        this.userRepository = userRepository;
        this.ownerRepository = ownerRepository;
        this.companyRepository = companyRepository;
        this.placeRepository = placeRepository;
        this.reservationRepository = reservationRepository;
        this.noteRepository = noteRepository;
        this.companyTypeRepository = companyTypeRepository;
        this.companyImageRepository = companyImageRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        loadUsers();
        loadCompanies();
        loadNotes();
        loadReservations();
        loadPlaces();
    }

    private void loadUsers(){
        if(userRepository.findAll().isEmpty()){
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            User user1 = new User();
            user1.setId(1L);
            user1.setEmail("user1@gmail.com");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
            user1.setBirthDate(LocalDateTime.of(1963,3,14,0,0));
            user1.setFirstName("Marry");
            user1.setLastName("Poppins");
            user1.setPassword(encoder.encode("user"));
            user1.setType("owner");
            user1.setImage("https://m.media-amazon.com/images/M/MV5BYTEwYjVmMTgtNzU0NC00MGQ4LWJmNTYtMzY3MTI5NjM5ZDEwXkEyXkFqcGdeQXVyMzAzMTg1OQ@@._V1_.jpg");
            userRepository.save(user1);

            User user2 = new User();
            user2.setId(2L);
            user2.setEmail("user2@gmail.com");
            formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
            user2.setBirthDate(LocalDateTime.of(1990,5,31,0,0));
            user2.setFirstName("John");
            user2.setLastName("Simmons");
            user2.setPassword(encoder.encode("user"));
            user2.setType("guest");
            user2.setImage("https://m.media-amazon.com/images/M/MV5BYTEwYjVmMTgtNzU0NC00MGQ4LWJmNTYtMzY3MTI5NjM5ZDEwXkEyXkFqcGdeQXVyMzAzMTg1OQ@@._V1_.jpg");
            userRepository.save(user2);

            User user3 = new User();
            user3.setId(3L);
            user3.setEmail("user3@gmail.com");
            formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
            user3.setBirthDate(LocalDateTime.of(1989,5,10,0,0));
            user3.setFirstName("Hally");
            user3.setLastName("Berry");
            user3.setPassword(encoder.encode("user"));
            user3.setType("user");
            user3.setImage("https://m.media-amazon.com/images/M/MV5BYTEwYjVmMTgtNzU0NC00MGQ4LWJmNTYtMzY3MTI5NjM5ZDEwXkEyXkFqcGdeQXVyMzAzMTg1OQ@@._V1_.jpg");
            userRepository.save(user3);
        }
    }

    private void loadCompanies(){
        if(companyRepository.findAll().isEmpty()){

            User userOwner1 = userRepository.findById(1L).get();
            User userGuest1 = userRepository.findById(2L).get();

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
            String date = null;

            CompanyType companyType1 = new CompanyType();
            companyType1.setId(1L);
            companyType1.setName("Launge Bar");
            companyTypeRepository.save(companyType1);

            CompanyType companyType2 = new CompanyType();
            companyType2.setId(2L);
            companyType2.setName("Beer Bar");
            companyTypeRepository.save(companyType2);

            Place place = new Place();
            place.setId(1L);
            place.setName("Skopje");
            place.setAddress("1000");
            placeRepository.save(place);

            CompanyImage companyImage = new CompanyImage();
            companyImage.setId(1L);
            companyImage.setImageUrl("https://realgarden.se/images/silent_resturent.jpg");
//            companyImageRepository.save(companyImage);

            CompanyImage companyImage2 = new CompanyImage();
            companyImage2.setId(2L);
            companyImage2.setImageUrl("https://realgarden.se/images/silent_resturent.jpg");
//            companyImageRepository.save(companyImage2);


            Company company1 = new Company();
            company1.setId(1L);
            company1.setName("Trend");
            company1.setAddress("bul.Jane Sandanski 55");
            company1.setCapacity(332);
            company1.setDescription("New Launge Bar in Skopje");
            company1.setCompanyType(companyType1);
            company1.setOwner(userOwner1);
            company1.setPlace(place);
            company1.setWorkingDaysMask("Working mask eheey");
            companyRepository.save(company1);
            company1.getImages().add(companyImage);
            companyImage.setCompany(company1);
            companyImageRepository.save(companyImage);


            Company company2 = new Company();
            company2.setId(2L);
            company2.setName("Beer Barr");
            company2.setAddress("bul.Jane Sandanski 10004");
            company2.setCapacity(332);
            company2.setDescription("Guitar perfect");
            company2.setCompanyType(companyType2);
            company2.setOwner(userOwner1);
            company2.setPlace(place);
            company2.setWorkingDaysMask("New working mask for the company to test");
            companyRepository.save(company2);
            companyImage2.setCompany(company2);
            company2.getImages().add(companyImage2);
            companyImageRepository.save(companyImage2);

            Reservation reservation = new Reservation();
            reservation.setId(1L);
            reservation.setPersonCount(3);
            //local date set in DB
            reservation.setCanceled(false);
            reservation.setRemark("123123");
            reservation.setUser(userGuest1);
            reservation.setCompany(company1);
            reservation.setForDate(LocalDateTime.now());
            reservationRepository.save(reservation);

            Reservation reservation2 = new Reservation();
            reservation2.setId(2L);
            reservation2.setPersonCount(3);
            //local date set in DB
            reservation2.setCanceled(false);
            reservation2.setRemark("alallala");
            reservation2.setUser(userGuest1);
            reservation2.setCompany(company2);
            reservation2.setForDate(LocalDateTime.now());
            reservationRepository.save(reservation2);

            Note note1= new Note();
            note1.setId(1L);
            note1.setSubject("This is the first note");
            note1.setDescription("hdjkashdjkahsdkjhaskjdhaskjdhkajshdjkashdkjhaskjdhaskj");
            note1.setCompany(company1);
            note1.setUser(userGuest1);
            noteRepository.save(note1);

//            Note note2= new Note();
//            note2.setId(2L);
//            note1.setSubject("This is the SECOND note");
//            note1.setDescription("hdjkashdjkahsdkjhaskjdhaskjdhkajshdjkashdkjhaskjdhaskj");
//            note1.setCompany(company2);
//            note1.setUser(userGuest1);
//            noteRepository.save(note2);

        }

    }
    private void loadReservations(){}
    private void loadNotes(){}
    private void loadPlaces(){}


}
