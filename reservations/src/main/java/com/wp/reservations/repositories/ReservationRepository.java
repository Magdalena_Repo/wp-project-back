package com.wp.reservations.repositories;

import com.wp.reservations.domain.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
      List<Reservation> findByCompany_Id(Long companyId);
      List<Reservation> findByCompany_IdAndForDate(Long companyId, LocalDateTime date);
      List<Reservation> findByUser_Id(Long userId);
}
