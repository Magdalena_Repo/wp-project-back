package com.wp.reservations.repositories;

import com.wp.reservations.domain.CompanyImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyImageRepository extends JpaRepository<CompanyImage, Long> {
}
