package com.wp.reservations.model;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class CompanyDTO {

    private Long id;
    private String name;
    private String address;
    private String description;
    private Integer capacity;
    private String workingDaysMask;
    private Long placeId;
    private Set<CompanyImageDTO> images = new HashSet<>();
    private Long companyTypeId;
    private Set<NoteDTO> notes =  new HashSet<>();
    private Long ownerId;
    private Set<ReservationDTO> reservations =  new HashSet<>();

}
