package com.wp.reservations.model;

import lombok.Data;

@Data
public class CompanyTypeDTO {

    private Long id;
    private String name;
}
