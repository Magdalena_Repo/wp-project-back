package com.wp.reservations.model;

import lombok.Data;

@Data
public class NoteDTO {
    private Long id;
    private String subject;
    private String description;
    private Long userId;
    private Long companyId;
    private String companyName;

}
