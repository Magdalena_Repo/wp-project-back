package com.wp.reservations.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
public class OwnerDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDateTime birthDate;
    private String password;
    private String type;
    private String image;
    private Set<CompanyDTO> ownedCompanies = new HashSet<>();
}
