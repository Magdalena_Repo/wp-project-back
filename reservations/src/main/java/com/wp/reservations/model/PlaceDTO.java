package com.wp.reservations.model;

import lombok.Data;

@Data
public class PlaceDTO {

    private Long id;
    private String name;
    private String address;
}
