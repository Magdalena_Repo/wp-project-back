package com.wp.reservations.model;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class ReservationDTO {

    private Long id;
    private String remark;
    private Integer personCount;
    private LocalDateTime forDate;
    private Long userId;
    private String userEmail;
    private String userFirstName;
    private String userLastName;
    private Long companyId;
    private String companyName;
    private String companyAddress;
}
