package com.wp.reservations.model;

import lombok.Data;

@Data
public class CompanyImageDTO {

    private Long id;
    private String imageUrl;
    private Long companyId;
}
