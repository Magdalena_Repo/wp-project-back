package com.wp.reservations.model;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
public class UserDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private LocalDateTime birthDate;
    private String email;
    private String password;
    private Long placeId;
    private String type;
    private String image;
    private Set<NoteDTO> notes =  new HashSet<>();
    private Set<ReservationDTO> reservations =  new HashSet<>();
}
