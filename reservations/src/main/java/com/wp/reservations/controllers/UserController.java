package com.wp.reservations.controllers;

import com.wp.reservations.domain.Note;
import com.wp.reservations.model.NoteDTO;
import com.wp.reservations.model.UserDTO;
import com.wp.reservations.services.MyUserDetailsService;
import com.wp.reservations.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;

    }

    @GetMapping(value = "/all")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDTO> getUsers() {
        return userService.getAllUsers();
    }

    @GetMapping(value = "/notes/all")
    @ResponseStatus(HttpStatus.OK)
    public List<NoteDTO> getNotesFromAllUsers() {
        return userService.getAllNotes();
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public UserDTO getUserByEmail(@RequestParam  String userEmail) {
        return userService.getAllUsers().stream().filter(x -> x.getEmail().equals(userEmail)).findFirst().orElse(null);
    }

    @GetMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public  UserDTO getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public Boolean login(@RequestBody UserDTO userDTO){
        return  this.userService.loginUser(userDTO);

    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO register(@RequestBody UserDTO userDTO){

        return this.userService.registerUser(userDTO);
    }

}
