package com.wp.reservations.controllers;

import com.wp.reservations.mappers.CompanyMapper;
import com.wp.reservations.model.CompanyDTO;
import com.wp.reservations.model.CompanyTypeDTO;
import com.wp.reservations.model.PlaceDTO;
import com.wp.reservations.repositories.CompanyRepository;
import com.wp.reservations.services.CompanyService;
import com.wp.reservations.services.CompanyTypeService;
import com.wp.reservations.services.PlaceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    private final CompanyRepository companyRepository;
    private final CompanyMapper companyMapper;
    private final CompanyService companyService;
    private final CompanyTypeService companyTypeService;
    private final PlaceService placeService;

    public CompanyController(CompanyRepository companyRepository, CompanyMapper companyMapper, CompanyService companyService, CompanyTypeService companyTypeService, PlaceService placeService) {
        this.companyRepository = companyRepository;
        this.companyMapper = companyMapper;
        this.companyService = companyService;
        this.companyTypeService = companyTypeService;
        this.placeService = placeService;
    }


    @GetMapping(value = "/all")
    @ResponseStatus(HttpStatus.OK)
    public List<CompanyDTO> getAllCompanies() {

        return companyService.getAllCompanies();
    }

    @GetMapping(value = "/{ownerId}")
    @ResponseStatus(HttpStatus.OK)
    public List<CompanyDTO> getAllCompaniesByOwner(@PathVariable Long ownerId) {

        return companyService.getAllCompaniesByOwner(ownerId);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CompanyDTO> getAllCompaniesByCompanyTypeName(@RequestParam String companyTypeName) {

        return companyService.getCompaniesByType(companyTypeName);
    }

    @GetMapping("/getAllByName")
    @ResponseStatus(HttpStatus.OK)
    public CompanyDTO getCompanyByName(@RequestParam String companyName) {

        return companyService.getCompanyByName(companyName);
    }


    @PostMapping(value="/create",produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyDTO createNewCompany(@RequestBody CompanyDTO companyDTO) {

        return companyService.createNewCompany(companyDTO);
    }

    @PutMapping(value = "{id}/update", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus(HttpStatus.OK)
    public CompanyDTO updateCompany(@PathVariable Long id, @RequestBody CompanyDTO companyDTO) {

        return companyService.updateCompany(id,companyDTO);
    }


    @PutMapping(value = "{id}/removeCompany")
    @ResponseStatus(HttpStatus.OK)
    public boolean removeCompany(@PathVariable Long id) {

        return companyService.removeCompany(id);
    }

    @GetMapping(value = "/types")
    @ResponseStatus(HttpStatus.OK)
    public List<CompanyTypeDTO> getAllCompanyTypes() {
        return companyTypeService.getCompanyTypes();
    }

    @GetMapping(value = "/places")
    @ResponseStatus(HttpStatus.OK)
    public List<PlaceDTO> getAllPlaces() {
        return placeService.getPlaces();
    }
}
