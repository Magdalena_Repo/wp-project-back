package com.wp.reservations.controllers;

import com.wp.reservations.model.OwnerDTO;
import com.wp.reservations.services.OwnerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/owners")
public class OwnerController {

    private final OwnerService ownerService;

    public OwnerController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public OwnerDTO getUserByEmail(@RequestParam String userEmail) {
        return ownerService.getOwnerByEmail(userEmail);
    }
}
