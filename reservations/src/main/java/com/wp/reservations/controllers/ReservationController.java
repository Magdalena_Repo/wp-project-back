package com.wp.reservations.controllers;


import com.wp.reservations.model.ReservationDTO;
import com.wp.reservations.services.ReservationService;
import com.wp.reservations.services.ReservationServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/reservations")
public class ReservationController {

    private final ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping(value = "/all")
    @ResponseStatus(HttpStatus.OK)
    public List<ReservationDTO> getAllReservations() {

        return reservationService.getAllReservations();
    }

    @GetMapping(value = "/company/{companyId}")
    @ResponseStatus(HttpStatus.OK)
    public List<ReservationDTO> getAllReservationsByCompany(@PathVariable Long companyId) {
        return reservationService.getAllReservationsByCompany(companyId);
    }

    @GetMapping(value = "/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public List<ReservationDTO> getAllReservationsByUser(@PathVariable Long userId) {
        return  reservationService.getAllReservationsByUser(userId);
    }

    @PostMapping(value="/{userId}/create",produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus(HttpStatus.CREATED)
    public ReservationDTO createNewReservation(@PathVariable Long userId, @RequestBody ReservationDTO reservationDTO) {
        return reservationService.reserve(reservationDTO,userId);
    }

    @PutMapping(value = "{id}/update", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus(HttpStatus.OK)
    public ReservationDTO updateReservation(@PathVariable Long id, @RequestBody ReservationDTO reservationDTO) {
        return reservationService.updateReservation(id, reservationDTO);
    }

    @PutMapping(value = "{id}/cancelReservation")
    @ResponseStatus(HttpStatus.OK)
    public ReservationDTO cancelReservation(@PathVariable Long id) {
        return reservationService.cancelReservation(id);
    }

    @DeleteMapping(value = "{id}/deleteReservation")
    @ResponseStatus(HttpStatus.OK)
    public ReservationDTO deleteReservation(@PathVariable Long id) {
        return reservationService.deleteReservation(id);
    }
}
