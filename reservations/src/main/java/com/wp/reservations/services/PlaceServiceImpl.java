package com.wp.reservations.services;

import com.wp.reservations.domain.Place;
import com.wp.reservations.mappers.PlaceMapper;
import com.wp.reservations.model.PlaceDTO;
import com.wp.reservations.repositories.PlaceRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlaceServiceImpl implements  PlaceService {

    private  final PlaceRepository placeRepository;
    private final PlaceMapper placeMapper;

    public PlaceServiceImpl(PlaceRepository placeRepository, PlaceMapper placeMapper) {
        this.placeRepository = placeRepository;
        this.placeMapper = placeMapper;
    }

    @Override
    public List<PlaceDTO> getPlaces() {
        List<Place> places =  placeRepository.findAll();
        return places.stream().map(x -> placeMapper.placeToPlaceDTO(x)).collect(Collectors.toList());
    }

    @Override
    public PlaceDTO savePlace(PlaceDTO placeDTO) {
        Place place = new Place();
        place.setName(placeDTO.getName());
        place.setAddress(placeDTO.getAddress());
        placeRepository.save(place);
        return placeMapper.placeToPlaceDTO(place);
    }
}
