package com.wp.reservations.services;

import com.wp.reservations.domain.*;
import com.wp.reservations.mappers.CompanyMapper;
import com.wp.reservations.model.CompanyDTO;
import com.wp.reservations.model.CompanyImageDTO;
import com.wp.reservations.repositories.*;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;
    private  final CompanyMapper companyMapper;
    private final UserRepository userRepository;
    private final CompanyTypeRepository companyTypeRepository;
    private final PlaceRepository placeRepository;
    private final CompanyImageRepository companyImageRepository;

    public CompanyServiceImpl(CompanyRepository companyRepository, CompanyMapper companyMapper, UserRepository userRepository, CompanyTypeRepository companyTypeRepository, PlaceRepository placeRepository, CompanyImageRepository companyImageRepository) {
        this.companyRepository = companyRepository;
        this.companyMapper = companyMapper;
        this.userRepository = userRepository;
        this.companyTypeRepository = companyTypeRepository;
        this.placeRepository = placeRepository;
        this.companyImageRepository = companyImageRepository;
    }

    @Override
    public CompanyDTO createNewCompany(CompanyDTO companyDTO) {
        Company company = new Company();
        company.setName(companyDTO.getName());
        company.setAddress(companyDTO.getAddress());
        company.setDescription(companyDTO.getDescription());
        company.setDeleted(false);
        company.setWorkingDaysMask(companyDTO.getWorkingDaysMask());
        company.setCapacity(companyDTO.getCapacity());
        company.setNotes(new HashSet<>());
        Optional<CompanyType> companyTypeOptional = companyTypeRepository.findById(companyDTO.getCompanyTypeId());
        if(!companyTypeOptional.isPresent()){
            throw new NullPointerException();
        }
        company.setCompanyType(companyTypeOptional.get());
        Optional<Place> placeOptional = placeRepository.findById(companyDTO.getPlaceId());
        if(!placeOptional.isPresent()){
            throw new NullPointerException();
        }
        company.setPlace(placeOptional.get());
        for(CompanyImageDTO companyImageDTO : companyDTO.getImages()){
            CompanyImage companyImage = new CompanyImage();
            companyImage.setImageUrl(companyImageDTO.getImageUrl());
            companyImage.setCompany(company);
            company.getImages().add(companyImage);
        }
        company.setReservations(new HashSet<>());
        Optional<User> user = userRepository.findById(companyDTO.getOwnerId());
        if(user.isPresent()){
            company.setOwner(user.get());
        } else {
            throw  new NullPointerException();
        }
        companyRepository.save(company);
        return companyMapper.companyToCompanyDTO(company);
    }

    @Override
    public CompanyDTO updateCompany(Long companyId, CompanyDTO companyDTO) {
        Optional<Company> companyOptional = companyRepository.findById(companyId);
        if(!companyOptional.isPresent()){
            throw new NullPointerException();
        }
        Company company = companyOptional.get();
        company.setName(companyDTO.getName());
        company.setAddress(companyDTO.getAddress());
        company.setDescription(companyDTO.getDescription());
        company.setWorkingDaysMask(companyDTO.getWorkingDaysMask());
        company.setCapacity(company.getCapacity());
        companyRepository.save(company);
        return companyMapper.companyToCompanyDTO(company);
    }


    @Override
    public boolean removeCompany(Long companyId) {
        Company company = companyRepository.findById(companyId).orElse(null);
        if(company != null){
            company.setDeleted(true);
            companyRepository.save(company);
            return true;
        }
        return false;
    }

    @Override
    public List<CompanyDTO> getAllCompaniesByOwner(Long id) {
        return companyRepository.findByOwner_Id(id).stream().map(companyMapper::companyToCompanyDTO).collect(Collectors.toList());
    }

    @Override
    public List<CompanyDTO> getAllCompanies() {
        List<Company> companies= companyRepository.findAll();
        return companies.stream().map(x -> companyMapper.companyToCompanyDTO(x)).collect(Collectors.toList());
    }

    @Override
    public CompanyDTO getCompanyByName(String name) {
        Company company= companyRepository.findByName(name).orElse(null);
        if(company != null){
            return companyMapper.companyToCompanyDTO(company);
        }
        return null;
    }

    @Override
    public CompanyDTO getCompanyById(Long id) {
        Company company= companyRepository.findById(id).orElse(null);
        if(company != null){
            return companyMapper.companyToCompanyDTO(company);
        }
        return null;
    }

    @Override
    public List<CompanyDTO> getCompaniesByType(String companyTypeName) {
        List<Company> companies= companyRepository.findByCompanyType_Name(companyTypeName);
        if(companies != null && companies.size() > 0){
            return companies.stream()
                    .filter(x -> !x.getDeleted())
                    .map(x -> companyMapper.companyToCompanyDTO(x))
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<CompanyDTO> getCompaniesByPlace(String placeName) {
        List<Company> companies= companyRepository.findByPlace_Name(placeName);
        if(companies != null && companies.size() > 0){
            return companies
                    .stream().filter(x -> !x.getDeleted())
                    .map(x -> companyMapper.companyToCompanyDTO(x))
                    .collect(Collectors.toList());
        }
        return null;
    }

}
