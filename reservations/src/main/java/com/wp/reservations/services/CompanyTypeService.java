package com.wp.reservations.services;

import com.wp.reservations.model.CompanyTypeDTO;

import java.util.List;

public interface CompanyTypeService {

    List<CompanyTypeDTO> getCompanyTypes();
    CompanyTypeDTO saveCompanyType(CompanyTypeDTO companyType);
}