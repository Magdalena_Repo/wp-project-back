package com.wp.reservations.services;


import com.wp.reservations.model.CompanyDTO;

import java.util.List;

public interface CompanyService {

    CompanyDTO createNewCompany(CompanyDTO companyDTO);
    CompanyDTO updateCompany(Long comapnyId,CompanyDTO companyDTO);
    boolean removeCompany(Long companyId);
    List<CompanyDTO> getAllCompaniesByOwner(Long id);
    List<CompanyDTO> getAllCompanies();
    CompanyDTO getCompanyByName(String name);
    CompanyDTO getCompanyById(Long id);
    List<CompanyDTO> getCompaniesByType(String companyTypeName);
    List<CompanyDTO> getCompaniesByPlace(String placeName);
}
