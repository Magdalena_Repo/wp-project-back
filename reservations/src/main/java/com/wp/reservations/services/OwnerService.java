package com.wp.reservations.services;

import com.wp.reservations.model.OwnerDTO;

public interface OwnerService {

    OwnerDTO getOwnerByEmail(String email);
}
