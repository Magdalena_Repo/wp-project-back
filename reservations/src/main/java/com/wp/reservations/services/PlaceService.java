package com.wp.reservations.services;


import com.wp.reservations.model.PlaceDTO;

import java.util.List;

public interface PlaceService {

    List<PlaceDTO> getPlaces();
    PlaceDTO savePlace(PlaceDTO place);
}
