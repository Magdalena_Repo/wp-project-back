package com.wp.reservations.services;

import com.wp.reservations.mappers.OwnerMapper;
import com.wp.reservations.model.OwnerDTO;
import com.wp.reservations.repositories.OwnerRepository;
import org.springframework.stereotype.Service;

@Service
public class OwnerServiceImpl implements OwnerService {
    private final OwnerRepository ownerRepository;
    private final OwnerMapper ownerMapper;

    public OwnerServiceImpl(OwnerRepository ownerRepository, OwnerMapper ownerMapper) {
        this.ownerRepository = ownerRepository;
        this.ownerMapper = ownerMapper;
    }

    @Override
    public OwnerDTO getOwnerByEmail(String email) {
        return ownerRepository.findByEmail(email).map(ownerMapper::ownerToOwnerDTO).orElse(null);
    }
}
