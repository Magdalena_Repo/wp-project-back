package com.wp.reservations.services;

import com.wp.reservations.domain.Company;
import com.wp.reservations.domain.Reservation;
import com.wp.reservations.domain.User;
import com.wp.reservations.mappers.ReservationMapper;
import com.wp.reservations.model.ReservationDTO;
import com.wp.reservations.repositories.CompanyRepository;
import com.wp.reservations.repositories.ReservationRepository;
import com.wp.reservations.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService{

    private final ReservationRepository reservationRepository;
    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;
    private final ReservationMapper reservationMapper;


    public ReservationServiceImpl(ReservationRepository reservationRepository, CompanyRepository companyRepository, UserRepository userRepository, ReservationMapper reservationMapper) {
        this.reservationRepository = reservationRepository;
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.reservationMapper = reservationMapper;
    }

    @Override
    public List<ReservationDTO> getAllReservationsByCompany(Long companyId) {
        List<Reservation> reservations = reservationRepository.findByCompany_Id(companyId);
        return reservations
                .stream()
                .map(reservation -> reservationMapper.reservationToReservationDTO(reservation))
                .collect(Collectors.toList());
    }

    @Override
    public List<ReservationDTO> getAllReservationsByUser(Long userId) {
        List<Reservation> reservations = reservationRepository.findByUser_Id(userId);
        return reservations
                .stream()
                .map(reservation -> reservationMapper.reservationToReservationDTO(reservation))
                .collect(Collectors.toList());
    }

    @Override
    public List<ReservationDTO> getAllReservations() {
        List<Reservation> reservations = reservationRepository.findAll();
        return reservations
                .stream()
                .map(reservation -> reservationMapper.reservationToReservationDTO(reservation))
                .collect(Collectors.toList());
    }

    @Override
    public ReservationDTO createNewReservation(ReservationDTO reservationDTO) {
        return null;
    }

    @Override
    public ReservationDTO updateReservation(Long reservationId, ReservationDTO reservationDTO) {
        Reservation reservation = reservationRepository.findById(reservationDTO.getId()).orElse(null);
        if(reservation != null){
            reservation.setForDate(LocalDateTime.from(reservationDTO.getForDate()));
            reservation.setPersonCount(reservationDTO.getPersonCount());
            reservationRepository.save(reservation);
            return reservationMapper.reservationToReservationDTO(reservation);
        }
        return null;
    }

    @Override
    public ReservationDTO cancelReservation(Long reservationId) {
        Reservation reservation = reservationRepository.findById(reservationId).orElse(null);
        if(reservation != null){
            reservation.setCanceled(true);
            reservationRepository.save(reservation);
            return reservationMapper.reservationToReservationDTO(reservation);
        }
        return null;
    }

    @Override
    public ReservationDTO deleteReservation(Long reservationId) {
        Reservation reservation = reservationRepository.findById(reservationId).orElse(null);
        if(reservation != null){;
            reservationRepository.delete(reservation);
        }
        return null;
    }

    @Override
    public List<ReservationDTO> getCompanyReservationsOnDate(Long companyId, LocalDateTime date){
        List<Reservation> reservations = reservationRepository.findByCompany_IdAndForDate(companyId, date);
        return reservations
                .stream()
                .map(reservation -> reservationMapper.reservationToReservationDTO(reservation))
                .collect(Collectors.toList());
    }

    @Override
    public ReservationDTO reserve(ReservationDTO reservationDTO, Long userId) {
        List<ReservationDTO> reservations = getCompanyReservationsOnDate(reservationDTO.getCompanyId(), reservationDTO.getForDate());
        int takenSeats = countTakenSeats(reservations);
        Company company = companyRepository.findById(reservationDTO.getCompanyId()).orElse(null);
        if (reservationDTO.getPersonCount() + takenSeats <= company.getCapacity()) {
            LocalDateTime date = LocalDateTime.from(reservationDTO.getForDate());
            Reservation reservation = new Reservation();
            reservation = createNewReservation(reservation,reservationDTO);
            User user= userRepository.findById(userId).orElse(null);
            if(user != null){
                reservation.setUser(user);
            }
            reservationRepository.save(reservation);
            return reservationMapper.reservationToReservationDTO(reservation);
        }
        return null;
    }

    private Reservation createNewReservation(Reservation reservation, ReservationDTO reservationDTO){
        Company company = companyRepository.findById(reservationDTO.getCompanyId()).orElse(null);
        reservation.setPersonCount(reservation.getPersonCount());
        reservation.setCompany(company);
        reservation.setForDate(reservationDTO.getForDate());
        reservation.setRemark(reservationDTO.getRemark());
        return reservation;
    }
    private int countTakenSeats(List<ReservationDTO> reservations) {
        return reservations.stream()
                .mapToInt(ReservationDTO::getPersonCount)
                .sum();
    }

    @Override
    public List<ReservationDTO> getAllCancelledReservationsByUser(Long userId) {
        List<Reservation> reservations = reservationRepository.findByUser_Id(userId);
        return reservations
                .stream().filter(x -> x.getCanceled())
                .map(reservation -> reservationMapper.reservationToReservationDTO(reservation))
                .collect(Collectors.toList());
    }

    @Override
    public List<ReservationDTO> getAllCancelledReservationsByCompany(Long companyId) {
        List<Reservation> reservations = reservationRepository.findByCompany_Id(companyId);
        return reservations
                .stream().filter(x -> x.getCanceled())
                .map(reservation -> reservationMapper.reservationToReservationDTO(reservation))
                .collect(Collectors.toList());
    }
}
