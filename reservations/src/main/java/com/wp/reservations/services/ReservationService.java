package com.wp.reservations.services;

import com.wp.reservations.domain.Reservation;
import com.wp.reservations.model.ReservationDTO;

import java.time.LocalDateTime;
import java.util.List;

public interface ReservationService {

    List<ReservationDTO> getAllReservationsByCompany(Long companyId);
    List<ReservationDTO> getAllReservationsByUser(Long id);
    List<ReservationDTO> getAllReservations();
    ReservationDTO createNewReservation(ReservationDTO reservationDTO);
    ReservationDTO updateReservation(Long reservationId, ReservationDTO reservationDTO);
    ReservationDTO cancelReservation(Long reservationId);
    ReservationDTO deleteReservation(Long reservationId);
    List<ReservationDTO> getCompanyReservationsOnDate(Long companyId, LocalDateTime date);
    ReservationDTO reserve(ReservationDTO reservationDTO, Long userId);
    List<ReservationDTO> getAllCancelledReservationsByUser(Long userId);
    List<ReservationDTO> getAllCancelledReservationsByCompany(Long companyId);
}
