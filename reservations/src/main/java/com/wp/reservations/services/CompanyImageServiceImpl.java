package com.wp.reservations.services;


import com.wp.reservations.domain.Company;
import com.wp.reservations.domain.CompanyImage;
import com.wp.reservations.mappers.CompanyImageMapper;
import com.wp.reservations.model.CompanyImageDTO;
import com.wp.reservations.repositories.CompanyImageRepository;
import com.wp.reservations.repositories.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyImageServiceImpl implements  CompanyImageService{

    private final CompanyImageRepository companyImageRepository;
    private final CompanyRepository companyRepository;
    private final CompanyImageMapper companyImageMapper;


    public CompanyImageServiceImpl(CompanyImageRepository companyImageRepository, CompanyRepository companyRepository, CompanyImageMapper companyImageMapper) {
        this.companyImageRepository = companyImageRepository;
        this.companyRepository = companyRepository;
        this.companyImageMapper = companyImageMapper;
    }

    @Override
    public List<CompanyImageDTO> getImages() {
        List<CompanyImage> companyImages =  this.companyImageRepository.findAll();
        return companyImages.stream().map(x -> this.companyImageMapper.CompanyImageToCompanyImageDTO(x)).collect(Collectors.toList());
    }

    @Override
    public List<CompanyImageDTO> saveImage(List<CompanyImageDTO> images) {
        for(CompanyImageDTO companyImageDTO : images){
            CompanyImage companyImage = new CompanyImage();
            companyImage.setImageUrl(companyImageDTO.getImageUrl());
            Company company = this.companyRepository.findById(companyImageDTO.getCompanyId()).orElse(null);
            if(company != null){
                companyImage.setCompany(company);
            }
            companyImageRepository.save(companyImage);
        }
        return null;
    }
}
