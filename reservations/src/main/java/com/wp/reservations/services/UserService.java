package com.wp.reservations.services;

import com.wp.reservations.domain.User;
import com.wp.reservations.mappers.NoteMapper;
import com.wp.reservations.mappers.UserMapper;
import com.wp.reservations.model.NoteDTO;
import com.wp.reservations.model.UserDTO;
import com.wp.reservations.repositories.NoteRepository;
import com.wp.reservations.repositories.UserRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final NoteRepository noteRepository;
    private final NoteMapper noteMapper;


    public UserService(UserRepository userRepository, UserMapper userMapper, NoteRepository noteRepository, NoteMapper noteMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.noteRepository = noteRepository;
        this.noteMapper = noteMapper;
    }

    public List<UserDTO> getAllUsers() {
        return userRepository.findAll().stream()
                .map(userMapper::userToUserDTO)
                .collect(Collectors.toList());
    }

    public UserDTO getUserById(Long id) {
        return null;
    }

    public boolean loginUser(UserDTO userDTO) {
        Optional<User> userOptional = this.userRepository.findByEmail(userDTO.getEmail());

        if (userOptional.isPresent()) {

            return  true;
        }
        else{
            throw new UsernameNotFoundException(userDTO.getEmail());
        }
    }

    public UserDTO registerUser(UserDTO userDTO) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        User newUser = new User();
        newUser.setEmail(userDTO.getEmail());
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setType(userDTO.getType());
        newUser.setBirthDate(userDTO.getBirthDate());
        newUser.setPassword(encoder.encode(userDTO.getPassword()));
        userRepository.save(newUser);
        return userMapper.userToUserDTO(newUser);
    }

    public List<NoteDTO> getAllNotes(){
        return noteRepository.findAll().stream().map(noteMapper::noteToNoteDTO).collect(Collectors.toList());
    }
}
