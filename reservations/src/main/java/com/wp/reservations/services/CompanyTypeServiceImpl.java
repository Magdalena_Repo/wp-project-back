package com.wp.reservations.services;

import com.wp.reservations.domain.CompanyType;
import com.wp.reservations.mappers.CompanyTypeMapper;
import com.wp.reservations.model.CompanyTypeDTO;
import com.wp.reservations.repositories.CompanyTypeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyTypeServiceImpl implements CompanyTypeService {

    private final CompanyTypeRepository companyTypeRepository;
    private final CompanyTypeMapper companyTypeMapper;

    public CompanyTypeServiceImpl(CompanyTypeRepository companyTypeRepository, CompanyTypeMapper companyTypeMapper) {
        this.companyTypeRepository = companyTypeRepository;
        this.companyTypeMapper = companyTypeMapper;
    }

    @Override
    public List<CompanyTypeDTO> getCompanyTypes()
    {
        List<CompanyType> companyTypes =  companyTypeRepository.findAll();

        return companyTypes.stream().map(x -> companyTypeMapper.companyTypeToCompanyTypeDTO(x)).collect(Collectors.toList());
    }

    @Override
    public CompanyTypeDTO saveCompanyType(CompanyTypeDTO companyTypeDTO) {
        CompanyType companyType = new CompanyType();
        companyType.setName(companyTypeDTO.getName());
        companyTypeRepository.save(companyType);
        return companyTypeMapper.companyTypeToCompanyTypeDTO(companyType);
    }
}
