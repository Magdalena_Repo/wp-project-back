package com.wp.reservations.services;

import com.wp.reservations.model.CompanyImageDTO;

import java.util.List;

public interface CompanyImageService {
    List<CompanyImageDTO> getImages();
    List<CompanyImageDTO> saveImage(List<CompanyImageDTO> image);
}
