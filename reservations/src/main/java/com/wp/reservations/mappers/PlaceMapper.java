package com.wp.reservations.mappers;

import com.wp.reservations.domain.Place;
import com.wp.reservations.model.PlaceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PlaceMapper {

    PlaceMapper INSTANCE = Mappers.getMapper(PlaceMapper.class);

    PlaceDTO placeToPlaceDTO(Place place);

    Place placeDTOToPlace(PlaceDTO placeDTO);
}