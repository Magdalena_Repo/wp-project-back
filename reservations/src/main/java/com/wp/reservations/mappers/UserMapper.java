package com.wp.reservations.mappers;

import com.wp.reservations.domain.Reservation;
import com.wp.reservations.domain.User;
import com.wp.reservations.model.ReservationDTO;
import com.wp.reservations.model.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mappings({
            @Mapping(target = "placeId", source = "place.id")
    })
    UserDTO userToUserDTO(User user);

    User userDTOToUser(UserDTO userDTO);
}