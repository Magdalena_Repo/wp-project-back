package com.wp.reservations.mappers;


import com.wp.reservations.domain.CompanyType;
import com.wp.reservations.model.CompanyTypeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CompanyTypeMapper {
    CompanyTypeMapper INSTANCE = Mappers.getMapper(CompanyTypeMapper.class);


    CompanyTypeDTO companyTypeToCompanyTypeDTO(CompanyType companyType);

    CompanyType companyTypeDTOToCompanyType(CompanyTypeDTO companyTypeDTO);
}