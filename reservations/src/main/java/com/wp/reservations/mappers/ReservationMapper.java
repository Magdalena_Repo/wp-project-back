package com.wp.reservations.mappers;

import com.wp.reservations.domain.Reservation;
import com.wp.reservations.model.ReservationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import javax.persistence.ManyToOne;

@Mapper(uses ={CompanyMapper.class})
public interface ReservationMapper {

    ReservationMapper INSTANCE = Mappers.getMapper(ReservationMapper.class);
    @Mappings({
            @Mapping(target = "companyId", source = "company.id"),
            @Mapping(target = "userId", source = "user.id"),
            @Mapping(target = "userEmail", source = "user.email"),
            @Mapping(target = "userFirstName", source = "user.firstName"),
            @Mapping(target = "userLastName", source = "user.lastName"),
            @Mapping(target = "companyName", source = "company.name"),
            @Mapping(target = "companyAddress", source = "company.address"),
            @Mapping(target = "forDate" , source="forDate")
    })
    ReservationDTO reservationToReservationDTO(Reservation reservation);
    Reservation reservationDTOToReservation(ReservationDTO reservationDTO);
}
