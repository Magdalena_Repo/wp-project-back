package com.wp.reservations.mappers;


import com.wp.reservations.domain.Owner;
import com.wp.reservations.model.OwnerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OwnerMapper {

    OwnerMapper INSTANCE = Mappers.getMapper(OwnerMapper.class);

    OwnerDTO ownerToOwnerDTO(Owner owner);

    Owner ownerDTOToOwner(OwnerDTO ownerDTO);
}