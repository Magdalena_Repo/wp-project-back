package com.wp.reservations.mappers;

import com.wp.reservations.domain.CompanyImage;
import com.wp.reservations.model.CompanyImageDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CompanyImageMapper {
    CompanyImageMapper INSTANCE = Mappers.getMapper(CompanyImageMapper.class);

    @Mappings({
            @Mapping(target = "companyId", source = "company.id")
    })
    CompanyImageDTO CompanyImageToCompanyImageDTO(CompanyImage companyImage);
    CompanyImage companyImageDTOToCompanyImage(CompanyImageDTO companyImageDTO);

}
