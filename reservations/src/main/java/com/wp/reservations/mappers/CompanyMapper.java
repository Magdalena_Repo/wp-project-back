package com.wp.reservations.mappers;


import com.wp.reservations.domain.Company;
import com.wp.reservations.model.CompanyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;


@Mapper(uses = {CompanyImageMapper.class})
public interface CompanyMapper {
    CompanyMapper INSTANCE = Mappers.getMapper(CompanyMapper.class);

    @Mappings({
            @Mapping(target = "companyTypeId", source = "companyType.id"),
            @Mapping(target = "ownerId", source = "owner.id"),
            @Mapping(target = "placeId", source = "place.id")
    })
    CompanyDTO companyToCompanyDTO(Company company);

    CompanyDTO companyDTOToCompany(CompanyDTO companyDTO);
}
