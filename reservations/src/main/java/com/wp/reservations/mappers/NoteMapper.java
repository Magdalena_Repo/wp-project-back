package com.wp.reservations.mappers;

import com.wp.reservations.domain.Note;
import com.wp.reservations.model.NoteDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface NoteMapper {
    NoteMapper INSTANCE = Mappers.getMapper(NoteMapper.class);

    @Mappings({
            @Mapping(target = "companyId", source = "company.id"),
            @Mapping(target = "userId", source = "user.id"),
            @Mapping(target = "companyName", source = "company.name")
    })
    NoteDTO noteToNoteDTO(Note note);

    Note noteDTOToNote(NoteDTO noteDTO);
}
